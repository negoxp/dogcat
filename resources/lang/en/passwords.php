<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Tu contraseña debe de ser minimo 6 caracteres',
    'reset' => 'Tu contraseña ha sido actualizada',
    'sent' => 'Te hemos mandado un email para recuperar tu contraseña',
    'token' => 'El token no es valido.',
    'user' => "No hemos podido encontrar el email en nuestra base de datos",

];
