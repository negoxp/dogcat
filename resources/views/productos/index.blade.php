@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-sm-12">
      <section class="panel">
          <header class="panel-heading">
              <i class="fa fa-th"></i> Productos
              <span class="tools pull-right">
                  <a class="btn btn-info pull-right" href="{{ url('/productos/create') }}" style="color:#fff;"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <!--
                  <a href="javascript:;" class="fa fa-cog"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                	-->
               </span>
          </header>
          <div class="panel-body">
          <div class="adv-table">
          <table  class="display table table-bordered table-striped dynamic-table">
	          <thead>
	          <tr>
					  	<th>Id</th>
					  	<th>Producto/Servico</th>
					  	<th>Costo</th>
					  	<th></th>
				  	</tr>
	          </thead>
	          <tbody>
	          	@foreach($productos as $p)
								<tr>
									<td>{{$p->id}}</td>
									<td>{{$p->nombre}}</td>
									<td>{{number_format($p->costo_neto,2)}}</td>
									<td>
											<a class="btn btn-primary pull-right" href="{{ url('/productos/'.$p->id.'/edit') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
									</td>
								</tr>
							@endforeach
	          </tbody>
	          <tfoot>
	          <tr>
					  	<th>Id</th>
					  	<th>Producto/Servico</th>
					  	<th>Costo</th>
					  	<th></th>
				  	</tr>
	          </tfoot>
          </table>
          </div>
          </div>
      </section>
  </div>
</div>





@endsection