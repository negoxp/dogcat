@extends('layouts.admin')

@section('content')

<a href="{{url('productos')}}" class="btn btn-info pull-right">Regresar <span class="icon icon-arrow-thin-left"></span></a>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<section class="panel">
    <header class="panel-heading">
        Producto {{$producto->id}}
    </header>
    <div class="panel-body">
        <div class="position-center">

        		@if( isset($model) )
						    {{ Form::model($producto, array('route' => array('productos.update', $producto->id), 'method' => 'PUT',  'files' => 'true')) }}
						@else
						    {{ Form::open(array('url' => 'productos', 'files' => true)) }}
						@endif
            <div class="form-group">
                {{ Form::label('nombre', 'Nombre') }} 
                {{ Form::text('nombre', $producto->nombre, array('class' => 'form-control', 'required' => 'required' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('costo_neto', 'Costo') }} 
                {{ Form::text('costo_neto', $producto->costo_neto, array('class' => 'form-control' )) }}
            </div>

                
            {{ Form::submit('Guardar', array('class' => 'btn btn-info')) }}
        {{ Form::close() }}
        </div>

    </div>
</section>

@if( isset($model) )
  {{ Form::open(array('url' => 'productos/' . $model->id, 'class' => 'pull-right')) }}
      {{ Form::hidden('_method', 'DELETE') }}
      {{ Form::submit('Eliminar este cliente', array('class' => 'btn btn-danger')) }}
  {{ Form::close() }}
@endif


@endsection