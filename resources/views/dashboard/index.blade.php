@extends('layouts.admin')

@section('content')




<div class="row">
    <div class="col-md-8">
        
    	<section class="panel">
        <header class="panel-heading">
            Caja <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-cog"></a>
            <a href="javascript:;" class="fa fa-times"></a>
            </span>
        </header>
        <div class="panel-body" style="height: 420px;">
            <div class="" style="position: relative; width: auto; height: 300px;">
            	<div class="todo-action-bar">
                <div class="row">
                    
                
                    <div class="col-xs-4 todo-search-wrap pull-left">
                        <input type="text" class="form-control search todo-search " placeholder=" Search">
                    </div>
                </div>
            </div>
            <br/><br/>
            	<ul class="to-do-list ui-sortable" id="sortable-todo" style="overflow: hidden; width: auto; height: 300px;">
                
            		@foreach($productos as $p)

								<li class="clearfix">
                    
                    <p class="todo-title">
                    	<button type="button" class="btn btn-default btn-primary"><i class="fa fa-plus"></i></button>
                        {{$p->id}} - {{$p->nombre}} 
                    </p>
                    <div class="todo-actionlist pull-right clearfix">
                        <span class="pull-rig bar-label-value"> ${{number_format($p->costo_neto,2)}}</span>
                    </div>
                </li>
							@endforeach

                
            </ul>
            <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 38px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 262.391px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
            
        </div>
    </section>


    </div>
    <div class="col-md-4">
        <!--widget graph start-->
        <section class="panel">
            <div class="panel-body">
                <ul class="to-do-list ui-sortable" id="sortable-todo" style="overflow: hidden; width: auto; height: 300px;">
                	<li class="clearfix">
                    
                    <p class="todo-title">
                    	<button type="button" class="btn btn-default btn-warning"><i class="fa fa-minus"></i></button>
                        {{$p->id}} - {{$p->nombre}} 
                    </p>
                    <div class="todo-actionlist pull-right clearfix">
                        <span class="pull-rig bar-label-value"> ${{number_format($p->costo_neto,2)}}</span>
                    </div>
                	</li>
                <ul>
            </div>
        </section>
        <!--widget graph end-->
        <!--widget graph start-->
        <section class="panel">
            <div class="panel-body">
                <ul class="clearfix prospective-spark-bar">
                    <li class="pull-left spark-bar-label">
                        <strong>Total</strong>
                    </li>
                    <li class="pull-right spark-bar-label">
                        <span class="bar-label-value"> $18887</span>
                        <span>Prospective Label</span>
                    </li>
                </ul>
            </div>
        </section>
        <!--widget graph end-->
        <!--widget weather start-->
        <section class="weather-widget clearfix">
            <div class="pull-left weather-icon">
                <canvas id="icon1" width="60" height="60"></canvas>
            </div>
            <div>
                <ul class="weather-info">
                    <li class="weather-city">New York <i class="ico-location"></i></li>
                    <li class="weather-cent"><span>18</span></li>
                    <li class="weather-status">Rainy Day</li>
                </ul>
            </div>
        </section>
        <!--widget weather end-->
    </div>
</div>



@endsection