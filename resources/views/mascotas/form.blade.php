@extends('layouts.admin')

@section('content')

<script type="text/javascript">
function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          $('#blah').show();
          reader.onload = function (e) {
              $('#blah')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };

          reader.readAsDataURL(input.files[0]);

      }
  }
  $('#blah').hide();
</script>


<a href="{{url('mascotas')}}" class="btn btn-info pull-right">Regresar <span class="icon icon-arrow-thin-left"></span></a>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<section class="panel">
    <header class="panel-heading">
        Mascota {{$mascota->id}}
    </header>
    <div class="panel-body">
        <div class="position-center">

        		@if( isset($model) )
						    {{ Form::model($mascota, array('route' => array('mascotas.update', $mascota->id), 'method' => 'PUT',  'files' => 'true')) }}
						@else
						    {{ Form::open(array('url' => 'mascotas', 'files' => true)) }}
						@endif
            <div class="form-group">
                {{ Form::label('cliente_id', 'Cliente') }} 
                {{ Form::select('cliente_id', $clientes, $mascota->cliente_id, array('class' => 'form-control') ) }}
            </div>
            <div class="form-group">
                {{ Form::label('nombre', 'Nombre') }} 
                {{ Form::text('nombre', $mascota->nombre, array('class' => 'form-control', 'required' => 'required' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('tipo_mascota', 'Tipo Mascota') }} 
                {{ Form::select('tipo_mascota', ['Perro' =>'Perro', 'Gato'=>'Gato', 'Otro' => 'Otro'], $mascota->tipo_mascota, array('class' => 'form-control') ) }}
            </div>
            <div class="form-group">
                {{ Form::label('fecha_nac', 'Fecha Nacimiento') }} 
                {{ Form::text('fecha_nac', $mascota->fecha_nac, array('class' => 'form-control default-date-picker' )) }}
            </div>
            
            <div class="form-group">
            <div class="row">
              <div class="col-xs-6">
                <section class="panel">
                    <div class="panel-body">
                    	{{ Form::label('foto', 'Foto') }} 
                			<input name="foto" type="file" id="foto" class='form-control' accept='image/gif, image/jpeg, image/png' onchange='readURL(this);' >
                    </div>
                </section>
              </div>
              <div class="col-xs-6">
                  <section class="panel">
                      <div class="panel-body"><img id="blah" src="#" alt="image" /></div>
                  </section>
              </div>
            </div>

                
            		 
            </div>
            {{ Form::submit('Guardar', array('class' => 'btn btn-info')) }}
        {{ Form::close() }}
        </div>

    </div>
</section>

@if( isset($model) )
  {{ Form::open(array('url' => 'mascotas/' . $model->id, 'class' => 'pull-right')) }}
      {{ Form::hidden('_method', 'DELETE') }}
      {{ Form::submit('Eliminar esta Mascota', array('class' => 'btn btn-danger')) }}
  {{ Form::close() }}
@endif


@endsection