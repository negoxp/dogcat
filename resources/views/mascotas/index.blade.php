@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-sm-12">
      <section class="panel">
          <header class="panel-heading">
              <i class="fa fa-github-alt"></i> Mascotas
              <span class="tools pull-right">
                  <a class="btn btn-info pull-right" href="{{ url('/mascotas/create') }}" style="color:#fff;"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <!--
                  <a href="javascript:;" class="fa fa-cog"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                	-->
               </span>
          </header>
          <div class="panel-body">
          <div class="adv-table">
          <table  class="display table table-bordered table-striped dynamic-table">
	          <thead>
	          <tr>
					  	<th>Id</th>
					  	<th>Foto</th>
					  	<th>Mascota</th>
					  	<th>Cliente</th>
					  	<th>tipo</th>
					  	<th></th>
				  	</tr>
	          </thead>
	          <tbody>
	          	@foreach($mascotas as $m)
								<tr>
									<td>{{$m->id}}</td>
									<td>{{$m->foto}}</td>
									<td>{{$m->nombre}}</td>
									<td>{{$m->cliente_id}}</td>
									<td>{{$m->tipo_mascota}}</td>
									<td>
											<a class="btn btn-primary pull-right" href="{{ url('/mascotas/'.$m->id.'/edit') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
									</td>
								</tr>
							@endforeach
	          </tbody>
	          <tfoot>
	          <tr>
					  	<th>Id</th>
					  	<th>Foto</th>
					  	<th>Mascota</th>
					  	<th>Cliente</th>
					  	<th>tipo</th>
					  	<th></th>
				  	</tr>
	          </tfoot>
          </table>
          </div>
          </div>
      </section>
  </div>
</div>





@endsection