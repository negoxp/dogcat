@extends('layouts.admin')
@section('content')

<h1 class="page-header text-gray">
    {{$title}}




        {!! Form::open(array('route' => ['users.destroy', isset($id) ? $id : 0], 'method' => 'delete', 'onsubmit'=>'return confirm("Are you sure you want to delete?")')) !!}
        <div class="pull-right btn-group">
            <a href="{{url('users')}}" class="btn btn-default">Regresar <span class="icon   icon-arrow-thin-left"></span></a>
            @if($title=='Edit User')
                <button type="submit" class="btn btn-danger">Eliminar <span class="icon icon-cross"></span></button>
            @endif
        </div>
        {!! Form::close() !!}



</h1>

<!--
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
-->
{!! form($form) !!}

<script>
$(document).ready(function(){

    check();
    $('#role_id').change(function(){
        check();
    });

});

function check(){

    var role = $('#role_id').val();
    console.log(role);

    if(role=='1'){
        $('#restaurant_id').parent().show('slow');
        $('#district_id').parent().hide('slow');
    }

    if(role=='2'){
        $('#restaurant_id').parent().hide('slow');
        $('#district_id').parent().show('slow');
    }

    if(role=='3' || role=='4'){
        console.log("here");
        $('#restaurant_id').parent().hide('slow');
        $('#district_id').parent().hide('slow');
    }
}
</script>
@endsection