@extends('layouts.admin')

@section('content')



<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body profile-information">
               <div class="col-md-3">
                   <div class="profile-pic text-center">
                       <img src="{{ asset('') }}vendor/theme/images/jorge.jpg" alt=""/>
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="profile-desk">
                       <h1>Jorge Flores</h1>
                       <span class="text-muted" style="padding-bottom: 2px;">Nivel 4</span>


                        <div class="row" >
                            <div class="col-md-10">
                                <h3>Usuarios con Producto pagado</h3>
                                <p>
                                   <span class="label label-success" style="padding-bottom: 4px;"> 4 de 8 </span> 50%, Fecha limite de compra 30 Dic. 
                                    <br/>
                                   <span class="label label-primary" style="padding-bottom: 4px;"> 4 </span> Usuarios Nuevos    
                                </p>
                            </div>
                            <div class="col-md-2">
                                <br/>
                                <div class="p-collection">
                                    <span class="pc-epie-chart" data-percent="50">
                                    <span class="percent"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                       <a href="#" class="btn btn-primary">Editar Perfil</a>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="profile-statistics">
                       <h1>18</h1>
                       <p>Usuarios en tu red</p>
                       <h1>$120.00</h1>
                       <p>Comisiones/Mes</p>
                       <ul>
                           <li>
                               <a href="#">
                                   <i class="fa fa-facebook"></i>
                               </a>
                           </li>
                           <li class="active">
                               <a href="#">
                                   <i class="fa fa-twitter"></i>
                               </a>
                           </li>
                           <li>
                               <a href="#">
                                   <i class="fa fa-google-plus"></i>
                               </a>
                           </li>
                       </ul>
                   </div>
               </div>
            </div>
        </section> 
    </div>
    <div class="col-md-12">
       <section class="panel DocumentList" style="height: 800px; overflow: auto">
        
        <div class="tree">
          <ul>
            
          </ul>
        </div>


        <!--
        <div class="tree">
            <ul>
                <li>
                    <a href="#">
                       <div class="tree-pic tree-pic-paid text-center">
                            <img src="{{ asset('') }}vendor/theme/images/jorge.jpg" alt=""/>
                            <div> Jorge Flores </div>
                       </div>
                    </a>
                    <ul>
                        <li>
                            <a href="#">
                                <div class="tree-pic tree-pic-new text-center">
                                    <img src="{{ asset('') }}vendor/theme/images/scavatar1.jpg" alt=""/>
                                    <div> Sara Carrillo</div>
                               </div>
                            </a>
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="tree-pic tree-pic-new text-center">
                                            <img src="{{ asset('') }}vendor/theme/images/scavatar2.jpg" alt=""/>
                                            <div> Juan Flores</div>
                                       </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                        <li>
                                             <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="tree-pic tree-pic-new text-center">
                                            <img src="{{ asset('') }}vendor/theme/images/scavatar3.jpg" alt=""/>
                                            <div> Heriberto Garcia</div>
                                       </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <div class="tree-pic tree-pic-paid text-center">
                                    <img src="{{ asset('') }}vendor/theme/images/juanpablo.jpg" alt=""/>
                                    <div> Juan Pablo Rocha</div>
                               </div>
                            </a>
                            <ul>
                                <li><a href="#">
                                    <div class="tree-pic tree-pic-new tree-pic-paid text-center">
                                    <img src="{{ asset('') }}vendor/theme/images/scavatar4.jpg" alt=""/>
                                    <div> Manuel Garcia</div>
                               </div>
                                </a>
                                    <ul>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <a href="#">
                                        <div class="tree-pic tree-pic-paid text-center">
                                            <img src="{{ asset('') }}vendor/theme/images/scavatar5.jpg" alt=""/>
                                            <div> Veronica Ruiz</div>
                                       </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/scavatar6.jpg" alt=""/>
                                                    <div> Oscar Cervantes</div>
                                               </div>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="saborcolima_registro.html">
                                                        <div class="tree-pic text-center">
                                                            <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                            <div> </div>
                                                       </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="saborcolima_registro.html">
                                                        <div class="tree-pic text-center">
                                                            <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                            <div> </div>
                                                       </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>   
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        -->


       </section>
    </div>
    </div>


@endsection
