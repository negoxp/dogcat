@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-sm-12">
      <section class="panel">
          <header class="panel-heading">
              <i class="fa fa-users"></i> Clientes
              <span class="tools pull-right">
                  <a class="btn btn-info pull-right" href="{{ url('/clientes/create') }}" style="color:#fff;"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <!--
                  <a href="javascript:;" class="fa fa-cog"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                	-->
               </span>
          </header>
          <div class="panel-body">
          <div class="adv-table">
          <table  class="display table table-bordered table-striped dynamic-table">
	          <thead>
	          <tr>
					  	<th>Id</th>
					  	<th>nombre</th>
					  	<th>celular</th>
					  	<th>email</th>
					  	<th></th>
				  	</tr>
	          </thead>
	          <tbody>
	          	@foreach($clientes as $c)
								<tr>
									<td>{{$c->id}}</td>
									<td>{{$c->nombre}}</td>
									<td>{{$c->celular}}</td>
									<td>{{$c->email}}</td>
									<td>
											<a class="btn btn-primary pull-right" href="{{ url('/clientes/'.$c->id.'/edit') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
									</td>
								</tr>
							@endforeach
	          </tbody>
	          <tfoot>
	          <tr>
					  	<th>Id</th>
					  	<th>nombre</th>
					  	<th>celular</th>
					  	<th>email</th>
					  	<th></th>
				  	</tr>
	          </tfoot>
          </table>
          </div>
          </div>
      </section>
  </div>
</div>





@endsection