@extends('layouts.admin')

@section('content')

<script type="text/javascript">
function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          $('#blah').show();
          reader.onload = function (e) {
              $('#blah')
                  .attr('src', e.target.result)
                  .width(150)
                  .height(200);
          };

          reader.readAsDataURL(input.files[0]);

      }
  }
  $('#blah').hide();
</script>


<a href="{{url('clientes')}}" class="btn btn-info pull-right">Regresar <span class="icon icon-arrow-thin-left"></span></a>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<section class="panel">
    <header class="panel-heading">
        Cliente {{$cliente->id}}
    </header>
    <div class="panel-body">
        <div class="position-center">

        		@if( isset($model) )
						    {{ Form::model($cliente, array('route' => array('clientes.update', $cliente->id), 'method' => 'PUT',  'files' => 'true')) }}
						@else
						    {{ Form::open(array('url' => 'clientes', 'files' => true)) }}
						@endif
            <div class="form-group">
                {{ Form::label('nombre', 'Nombre') }} 
                {{ Form::text('nombre', $cliente->nombre, array('class' => 'form-control', 'required' => 'required' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('domicilio', 'Domicilio') }} 
                {{ Form::text('domicilio', $cliente->domicilio, array('class' => 'form-control' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('telefono', 'Telefono') }} 
                {{ Form::text('telefono', $cliente->telefono, array('class' => 'form-control' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('celular', 'Celular') }} 
                {{ Form::text('celular', $cliente->celular, array('class' => 'form-control' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('email', 'Email') }} 
                {{ Form::text('email', $cliente->email, array('class' => 'form-control' )) }}
            </div>
            <div class="form-group">
                {{ Form::label('modo_contacto', 'Modo de contacto') }} 
                {{ Form::select('modo_contacto', ['celular', 'sms', 'Whatsapp', 'telefono'], $cliente->modo_contacto, array('class' => 'form-control') ) }}
            </div>
            <div class="form-group">
                <div class="row">
                        <div class="col-xs-6">
                            <section class="panel">
                                <div class="panel-body">
                                	{{ Form::label('foto', 'Foto') }} 
                									<input name="foto" type="file" id="foto" class='form-control' accept='image/gif, image/jpeg, image/png' onchange='readURL(this);' >
                                </div>
                            </section>
                        </div>
                        <div class="col-xs-6">
                            <section class="panel">
                                <div class="panel-body"><img id="blah" src="#" alt="image" /></div>
                            </section>
                        </div>
                    </div>

                
            		 
            </div>
           @php 
           /*
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Check me out
                </label>
            </div>
           */
           @endphp
            {{ Form::submit('Guardar', array('class' => 'btn btn-info')) }}
        {{ Form::close() }}
        </div>

    </div>
</section>

@if( isset($model) )
  {{ Form::open(array('url' => 'clientes/' . $model->id, 'class' => 'pull-right')) }}
      {{ Form::hidden('_method', 'DELETE') }}
      {{ Form::submit('Eliminar este cliente', array('class' => 'btn btn-danger')) }}
  {{ Form::close() }}
@endif


@endsection