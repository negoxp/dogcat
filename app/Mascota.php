<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
  public function cliente()
  {
      return $this->belongsTo('App\Cliente');
  }
  
  public function pedidos(){ 
    return $this->hasMany('App\Pedido');
  }
}
