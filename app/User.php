<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract 
{

    use Authenticatable, CanResetPassword, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


    public function getPhoto(){
        //return file_exists(public_path().'/uploads/profile/'.$this->foto);

        if(file_exists(public_path().'/uploads/profile/'.$this->foto) and $this->foto!="" ){
            return asset('').'uploads/profile/'.$this->foto;
        }else{
            is_numeric($this->foto_default) ? $f=$this->foto_default : $f=1;
            return asset('').'vendor/theme/images/avatars/'.$f.'.png';
        }

        //if ($this->foto)
        //return asset('');//$this->name;
    }

    public function getPrintPerson($person=null){
        $person ? $me=$person : $me=$this;

        //calses de status 
        //tree-pic-paid tree-pic-new
        $output="
        <li>
          <a href='#''>
               <div class='tree-pic  text-center'>
                    <img src='".$me->getPhoto()."' alt=''/>
                    <div>".$me->name."</div>
               </div>
            </a>
            ".$this->getPrintSons($me)."
        </li>";
        return $output;
    }

    public function getPrintFreeSpot($parentId, $hijoPosicion){
        $output="
        <li>
          <a href='#' class='mymodal' hijoPosicion='".$hijoPosicion."' parentId='".$parentId."'>
               <div class='tree-pic'>
                    <img src='".asset('')."vendor/theme/images/newuser.png' alt=''/>
                    <div></div>
               </div>
            </a>
        </li>";

        return $output;
    }

    public function getPrintSons($me){
        $userid=$me->id;

        $condition="padre_id='$userid'";
        $sons = User::whereRaw($condition)->orderBy('hijo_posicion', 'asc')->limit(2)->get();
        $sons_output="";
        $i=1;

        foreach($sons as $s){
            if ($s->hijo_posicion==2 and count($sons)==1){
                $sons_output.=$this->getPrintFreeSpot($userid, $i);
                $i++;
            }
            $sons_output.=$this->getPrintPerson($s);
            $i++;
        }

        for ($i = $i; $i <= 2; $i++) {
            $sons_output.=$this->getPrintFreeSpot($userid, $i);
        }


        $output="
        <ul>
            ".$sons_output."
        </ul>
        ";
        return $output;
    }

    




}
