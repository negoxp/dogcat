<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Role;
use \DB;
class UserForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
        $this
            ->add('name','text', ['label'     => 'Nombre'])
            ->add('email','email')
            ->add('password', 'repeated', [
                'type' => 'password',    // can be anything that fits <input type="type-here">
                'second_name' => 'password_confirmation', // defaults to name_confirmation
                'first_options' => ['label' => 'Contraseña', 'value' => ''],   // Same options available as for text type
                'second_options'    => ['label' => 'Repetir Contraseña'],   // Same options available as for text type
                
            ])
            ->add('role_id', 'select', [
                'choices'   => ['1' => 'Administrador', '2' => 'Moderador de Pagos'],
                'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Rol'
            ])
            /*
            ->add('restaurant_id', 'select', [
                'choices'   => Restaurant::orderBy(DB::Raw('LENGTH(name), name'))->lists('name', 'id'),
                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Location'
            ])
            ->add('district_id', 'select', [
                'choices'   => District::all()->lists('name', 'id'),
                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'District'
            ])
            ->add('can_view_d9', 'checkbox', [

                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Can View D9'
            ])
            ->add('can_legal', 'checkbox', [

                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Can View Legal'
            ])
            */
            ->add('guardar', 'submit', [
                'attr' => ['class' => 'btn btn-primary']
            ])

        ;
    }
}