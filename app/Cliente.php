<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
  public function mascotas(){ 
    return $this->hasMany('App\Mascota');
  }

  public function pedidos(){ 
    return $this->hasMany('App\Pedido');
  }
}
