<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
  public function cliente()
  {
      return $this->belongsTo('App\Cliente');
  }

  public function mascota()
  {
      return $this->belongsTo('App\Mascota');
  }

  public function pedidosDetalles(){
      return $this->hasMany('App\PedidosDetalle');
  }
}
