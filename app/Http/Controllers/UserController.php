<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        //$users = User::with('roles')->get();
        $users = User::all();
        //return view('home');
        return view('users.index',compact("users"));
	}

		/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(FormBuilder $formBuilder)
	{
        $form = $formBuilder->create('App\Forms\UserForm',[
            'method'    =>  'POST',
            'url'       =>  route('users.store')
        ]);

        //dd(Role::all()->lists('display_name', 'id'));
		//
        return view('users.form', ['form'=>$form,'title'=>'New User']);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserFormRequest $request)
	{
		//
        $user = new User([
            'name'      =>  $request->get('name'),
            'email'     =>  $request->get('email'),
            'password'  =>  bcrypt($request->get('password')),
            //'restaurant_id'     =>  $request->get('restaurant_id',null),
            //'district_id'     =>  $request->get('district_id'.null)

        ]);

        //$user->roles()->getResults()
        $user->save();
        $user->roles()->sync([]);
        $user->roles()->attach($request->get('role_id'));
        return \Redirect::route('users.index')->with('success','User registered successfully');
	}

	 /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id, FormBuilder $formBuilder)
	{

		//
        $user = User::findOrFail($id);
        $form = $formBuilder->create('App\Forms\UserForm',[
            'model'     =>  $user,
            'method'    =>  'PUT',
            'url'       =>  route('users.update',['id'=>$user->id])
        ]);

        return view('users.form', ['form'=>$form,'title'=>'Edit User','id'=>$user->id]);

	}

		/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UserFormRequest $request)
	{
		//
        $user = User::find($id);
        $user->update([
            'name'      =>  $request->get('name'),
            'email'     =>  $request->get('email'),
            /*
            'restaurant_id'     =>  $request->get('restaurant_id',null),
            'district_id'     =>  $request->get('district_id',null),
            'can_view_d9'     =>  $request->get('can_view_d9'),
            'can_legal'     =>  $request->get('can_legal'),
            */
        ]);

        

        if($request->get('password'))
            $user->password  = bcrypt($request->get('password'));


        $user->save();

        $user->roles()->sync([]);
        $user->roles()->attach($request->get('role_id'));
        return \Redirect::route('users.index')->with('success','User updated successfully');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        User::destroy($id);
        return \Redirect::route('users.index')->with('success','User deleted successfully');
	}



}
