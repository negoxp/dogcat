<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
//use App\Http\Controllers\Session;

use App\Cliente;
use App\Producto;
use URL;
use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Session;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::all();

        return view('productos/index')->with([
            'productos'   =>  $productos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $producto = new Producto;

        $action = URL::route('productos.store');

        return view('productos/form')->with([
            'producto'   =>  $producto
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nombre'       => 'required',
            'costo_neto' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('productos/create')->withErrors($validator)->withInput();
        } else {
            $producto = new Producto;
            $producto->nombre   = Input::get('nombre');
            $producto->costo_neto= Input::get('costo_neto');
            $producto->save();

            // redirect
            Session::flash('message-success', 'Exito! El producto ha sido creado!');
            return Redirect::to('productos');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::findOrFail($id);

        return view('productos/form')->with([
            'producto'   =>  $producto,
            'model'     => $producto,
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $producto = Producto::findOrFail($id);
       $rules = array(
            'nombre'       => 'required',
            'costo_neto' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('productos/create')->withErrors($validator)->withInput();
        } else {
            $producto->nombre   = Input::get('nombre');
            $producto->costo_neto= Input::get('costo_neto');
            $producto->save();

            // redirect
            Session::flash('message-success', 'Exito! El producto ha sido Actualizado!');
            return Redirect::to('productos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::findOrFail($id);
        $producto->delete();

        Session::flash('message-warning', 'Exito! El producto has sido eliminado!');
        return Redirect::to('productos');
    }
}
