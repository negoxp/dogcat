<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;







class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo \Auth::user()->name;
        //echo \Auth::user()->hasRole('admin');
        //exit();

        $me=\Auth::user();
        $productos = Producto::all();

        return view('dashboard/index')->with([
            'me'   =>  $me,
            'productos'   =>  $productos,
            'success'   =>  "",
        ]);


        //return view('home');
    }


    public function invite(Request $request){

        $response = Password::sendResetLink(['email' => "negoxp@gmail.com"], function($message)
        {
            $message->subject('Bienvenido a Café Sabor a Colima');
        });
        exit();

        $now = new \DateTime();
        $token = str_random(10); 
        $user = new User([
            'name'              =>  shortname($request->get('nombres'), $request->get('apellidos')),
            'email'             =>  $request->get('email'),
            'password'          =>  bcrypt($now->getTimestamp())

        ]);
        $user->tipo="socio";
        $user->status="pendiente";
        $user->numero_membresia=md5( $now->getTimestamp());
        $user->nombre=$request->get('nombres');
        $user->apellidos=$request->get('apellidos');
        $user->padre_id=$request->get('parentId');
        $user->presentador_id=\Auth::user()->id;
        $user->genero=$request->get('genero');
        $user->foto_default=$request->get('foto_default');
        $user->hijo_posicion=$request->get('hijo_posicion');
        $user->remember_token = $token;

        //$user->roles()->getResults()
        $user->save();
        $user->roles()->sync([]);
        $user->roles()->attach(3);
        
        //Password::sendResetLink(['email' => 'negoxp@gmail.com', ]);

        $response = Password::sendResetLink(['email' => $user->email], function($message)
        {
            $message->subject('Bienvenido a Café Sabor a Colima');
        });

        /*Mail::send('users.mails.welcome', array('firstname'=>Input::get('firstname')), function($message){
        $message->to(Input::get('email'), Input::get('firstname').' '.Input::get('lastname'))->subject('Welcome to the Laravel 4 Auth App!');
        });*/

        return \Redirect::route('home.index')->with('success','User registered successfully');
    }
}

function shortname($names, $last)
{
    $nombres = explode(" ", $names);
    $apellidos = explode(" ", $last);
    return $nombres[0]." ".$apellidos[0];
}
