<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
//use App\Http\Controllers\Session;

use App\Cliente;
use App\Mascota;
use URL;
use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Session;

class MascotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $mascotas = Mascota::all();

        return view('mascotas/index')->with([
            'mascotas'   =>  $mascotas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mascota = new Mascota;
        $clientes = Cliente::lists('nombre', 'id');

        return view('mascotas/form')->with([
            'mascota'   =>  $mascota,
            'clientes'   =>  $clientes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
            'cliente_id'       => 'required',
            'nombre'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('mascotas/create')->withErrors($validator)->withInput();
        } else {
            $mascota = new Mascota;
            $mascota->cliente_id   = Input::get('cliente_id');
            $mascota->nombre= Input::get('nombre');
            $mascota->tipo_mascota = Input::get('tipo_mascota');
            $mascota->fecha_nac = Input::get('fecha_nac');
            $mascota->save();

            $file = Request::file('foto');
            if ($file){
                $path = '/uploads/mascotas/';
                $photoname = date('Ymd').'_'.$mascota->id.'_'.uniqid().'.'. $file->guessClientExtension();
                $path = public_path('/uploads/mascotas/' . $photoname);
                Image::make($file->getRealPath())->resize(300, 300)->save($path);
                $mascota->foto = $photoname;
                $mascota->save();
            }

            // redirect
            Session::flash('message-success', 'Exito! Mascota creado!');
            return Redirect::to('mascotas');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $mascota = Mascota::findOrFail($id);
       $clientes = Cliente::lists('nombre', 'id');

        return view('mascotas/form')->with([
            'mascota'   =>  $mascota,
            'model'     => $mascota,
            'clientes'   =>  $clientes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $mascota = Mascota::findOrFail($id);

       $rules = array(
            'cliente_id'       => 'required',
            'nombre'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('mascotas/create')->withErrors($validator)->withInput();
        } else {
            $mascota->cliente_id   = Input::get('cliente_id');
            $mascota->nombre= Input::get('nombre');
            $mascota->tipo_mascota = Input::get('tipo_mascota');
            $mascota->fecha_nac = Input::get('fecha_nac');
            $mascota->save();

            $file = Request::file('foto');
            if ($file){
                $path = '/uploads/mascotas/';
                $photoname = date('Ymd').'_'.$mascota->id.'_'.uniqid().'.'. $file->guessClientExtension();
                $path = public_path('/uploads/mascotas/' . $photoname);
                Image::make($file->getRealPath())->resize(300, 300)->save($path);
                $mascota->foto = $photoname;
                $mascota->save();
            }

            // redirect
            Session::flash('message-success', 'Exito! Mascota editado!');
            return Redirect::to('mascotas');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $mascota = Mascota::findOrFail($id);
        $mascota->delete();

        Session::flash('message-warning', 'Exito! Mascota eliminada!');
        return Redirect::to('mascotas');
    }
}
