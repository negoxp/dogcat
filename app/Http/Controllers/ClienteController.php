<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
//use App\Http\Controllers\Session;

use App\Cliente;
use URL;
use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Session;


class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();

        return view('clientes/index')->with([
            'clientes'   =>  $clientes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $cliente = new Cliente;

        $action = URL::route('clientes.store');

        return view('clientes/form')->with([
            'cliente'   =>  $cliente
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $rules = array(
            'nombre'       => 'required',
            'email'      => 'required|email',
            'celular' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('clientes/create')->withErrors($validator)->withInput();
        } else {
            $cliente = new Cliente;
            $cliente->nombre   = Input::get('nombre');
            $cliente->domicilio= Input::get('domicilio');
            $cliente->telefono = Input::get('telefono');
            $cliente->celular = Input::get('celular');
            $cliente->email = Input::get('email');
            $cliente->modo_contacto = Input::get('modo_contacto');
            $cliente->save();

            $file = Request::file('foto');
            if ($file){
                $path = '/uploads/clientes/';
                $photoname = date('Ymd').'_'.$cliente->id.'_'.uniqid().'.'. $file->guessClientExtension();
                $path = public_path('/uploads/clientes/' . $photoname);
                Image::make($file->getRealPath())->resize(300, 300)->save($path);
                $cliente->foto = $photoname;
                $cliente->save();
            }

            // redirect
            Session::flash('message-success', 'Exito! cliente creado!');
            return Redirect::to('clientes');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);

        return view('clientes/form')->with([
            'cliente'   =>  $cliente,
            'model'     => $cliente,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::findOrFail($id);

        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'nombre'       => 'required',
            'email'      => 'required|email',
            'celular' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('clientes/create')
                        ->withErrors($validator)
                        ->withInput();

        } else {
            $cliente->nombre   = Input::get('nombre');
            $cliente->domicilio= Input::get('domicilio');
            $cliente->telefono = Input::get('telefono');
            $cliente->celular = Input::get('celular');
            $cliente->email = Input::get('email');
            $cliente->modo_contacto = Input::get('modo_contacto');
            $cliente->save();

            $file = Request::file('foto');
            if ($file){
                $path = '/uploads/clientes/';
                $photoname = date('Ymd').'_'.$cliente->id.'_'.uniqid().'.'. $file->guessClientExtension();
                $path = public_path('/uploads/clientes/' . $photoname);
                Image::make($file->getRealPath())->resize(300, 300)->save($path);
                $cliente->foto = $photoname;
                $cliente->save();
            }

            Session::flash('message-success', 'Exito! cliente creado!');
            return Redirect::to('clientes');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();

        Session::flash('message-warning', 'Exito! cliente eliminado!');
        return Redirect::to('clientes');
    }
}
