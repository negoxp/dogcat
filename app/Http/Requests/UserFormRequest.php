<?php 

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UserFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        //dd($this->getSegmentFromEnd());
        $s = $this->getSegmentFromEnd();
        $user = User::find($this->users);
        $plus = $user ? ','.$user->id : '';

        $value="";
        if($s && $s !=='users'){
            $value = ',email,'.$s;
        }
        //dd($value);
		return [
			//
            'name'      =>  'required',
            'email'     =>  'required|email|unique:users,email'.$plus,
            'password'  =>  'same:password_confirmation',
            'role_id'   =>  'required|exists:roles,id'
		];
	}

    private function getSegmentFromEnd($position_from_end = 1) {
        $segments =$this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }

}
