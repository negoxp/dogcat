
$(function() {

  $('.dynamic-table').dataTable( {
      "aaSorting": [[ 4, "desc" ]]
  } );

  $('.default-date-picker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });

  $(".mymodal").click(function(){
  
		$("#parentId").val($(this).attr('parentId'));
		$("#hijo_posicion").val($(this).attr('hijoPosicion'));
               

   	$("#myModal").modal();
  });


});