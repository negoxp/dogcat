<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')
                ->references('id')->on('clientes')
                ->onDelete('cascade');
            $table->integer('mascota_id')->unsigned();
            $table->foreign('mascota_id')
                ->references('id')->on('mascotas')
                ->onDelete('cascade');
            $table->decimal('total',10,2);
            $table->string('status',30); //pagado-pendiente
            $table->boolean('pagado')->default(false);
            $table->timestamp('pagado_fecha');
            $table->boolean('autorizacion_user_id');
            $table->string('autorizacion_typo',30); //efectivo/tarjeta/credito..etc
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedidos');
    }
}
