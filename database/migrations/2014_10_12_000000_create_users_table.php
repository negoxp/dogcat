<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('status');
            $table->string('numero_membresia');
            $table->string('foto');
            $table->string('nombre');
            $table->string('apellidos');
            $table->date('fecha_nac');

            $table->string('telefono');
            $table->string('celular');

            $table->string('direccion');
            $table->string('colonia');
            $table->string('ciudad');
            $table->string('estado');
            $table->string('cp');

            $table->string('banco_nombre');
            $table->string('banco_titular');
            $table->string('banco_cuenta');
            $table->string('banco_clabe');

            $table->integer('padre_id');
            $table->integer('presentador_id');

            $table->date('ultimo_pedido');
            $table->boolean('pedido_mes_pagado')->default(false);
            $table->integer('red_integrantes');
            $table->integer('red_integrantes_pagados');
            $table->decimal('red_comision_pago',10,2);
            $table->timestamp('red_comision_fecha');

            $table->integer('red_ultima_integrantes');
            $table->integer('red_ultima_integrantes_pagados');
            $table->decimal('red_ultima_comision_pago',10,2);
            $table->date('red_ultima_comision_fecha');

            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
